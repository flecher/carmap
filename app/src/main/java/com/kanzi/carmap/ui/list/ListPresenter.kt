package com.kanzi.carmap.ui.list

import android.util.Log
import com.kanzi.carmap.data.Filters
import com.kanzi.carmap.data.model.VehicleListResponse
import com.kanzi.carmap.di.scope.PerView
import com.kanzi.carmap.ui.base.BasePresenter
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

@PerView
class ListPresenter : BasePresenter<ListView>() {

    companion object {
        val TAG = ListPresenter::class.java.simpleName
    }
    fun onResume() {
        Filters.carListFiltered
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        { onFetchSuccess(it) },
                        { onFetchError(it) }
                )
    }

    private fun onFetchError(throwable: Throwable) {
        Log.e(TAG, "Failure fetching data", throwable)
    }

    private fun onFetchSuccess(cars: List<VehicleListResponse>) {
        view?.showCarList(cars)
    }
}