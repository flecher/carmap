package com.kanzi.carmap.ui.list

import com.kanzi.carmap.data.model.VehicleListResponse
import com.kanzi.carmap.ui.base.MvpView

interface ListView : MvpView {

    fun showCarList(cars : List<VehicleListResponse>)

    fun onFetchError(error: Throwable)
}