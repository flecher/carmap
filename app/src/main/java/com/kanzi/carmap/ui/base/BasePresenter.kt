package com.kanzi.carmap.ui.base

import android.content.Context
import io.reactivex.disposables.CompositeDisposable

abstract class BasePresenter<T> {

    protected val disposables = CompositeDisposable()
    protected var view: T? = null
    protected var context: Context? = null

    fun bind(view: T, context: Context) {
        this.view = view
        this.context = context
    }

    private fun unbind() {
        this.view = null
    }

    fun destroy() {
        disposables.clear()
        unbind()
    }
}