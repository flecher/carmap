package com.kanzi.carmap.ui.map

import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.kanzi.carmap.R
import com.kanzi.carmap.data.model.VehicleListResponse
import com.kanzi.carmap.ui.base.BaseFragment

class MapFragment : BaseFragment(), OnMapReadyCallback, MapView {

    companion object {
        val TAG = MapFragment::class.java.simpleName
        const val mapZoomLevel = 12.0f
    }

    lateinit var presenter: MapPresenter

    override fun getLayoutResId(): Int = R.layout.map_fragment_layout

    private lateinit var mMap: GoogleMap

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        fragmentComponent?.inject(this)
        presenter = MapPresenter()
        presenter.bind(this, mRootView.context)
        val map = childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        map.getMapAsync(this)
    }

    override fun onMapReady(googleMap: GoogleMap?) {
        mMap = googleMap!!
        presenter.onResume()
    }

    override fun clearMap() {
        mMap.clear()
    }

    override fun showCarsOnMap(car: VehicleListResponse) {
        val latLng = LatLng(
                car.location.latitude,
                car.location.longitude)
        mMap.addMarker(
                MarkerOptions()
                        .position(latLng)
                        .title(car.model.title))
        mMap.moveCamera(
                CameraUpdateFactory
                        .newLatLngZoom(latLng, mapZoomLevel))
    }

    override fun onFetchError(error: Throwable) {
        Log.e(TAG, "Failure fechting data", error)
        Toast.makeText(mRootView.context, "Failure fetching data", Toast.LENGTH_LONG).show()
    }
}