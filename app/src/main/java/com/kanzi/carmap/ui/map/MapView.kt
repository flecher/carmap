package com.kanzi.carmap.ui.map

import com.kanzi.carmap.data.model.VehicleListResponse
import com.kanzi.carmap.ui.base.MvpView

interface MapView : MvpView {

    fun showCarsOnMap(cars: VehicleListResponse)

    fun onFetchError(error: Throwable)

    fun clearMap()
}