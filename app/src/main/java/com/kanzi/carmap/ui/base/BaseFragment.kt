package com.kanzi.carmap.ui.base

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.kanzi.carmap.di.component.FragmentComponent
import com.kanzi.carmap.utilities.extensions.getAppComponent

abstract class BaseFragment : Fragment() {

    internal lateinit var mRootView: View

    protected var fragmentComponent: FragmentComponent? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        mRootView = inflater!!.inflate(getLayoutResId(), container, false)
        fragmentComponent = mRootView.context.getAppComponent().fragmentComponent()
        return mRootView
    }


    protected abstract fun getLayoutResId(): Int
}