package com.kanzi.carmap.ui

import android.util.Log
import com.kanzi.carmap.data.DataManager
import com.kanzi.carmap.data.Filters
import com.kanzi.carmap.data.model.VehicleListResponse
import com.kanzi.carmap.di.scope.PerView
import com.kanzi.carmap.ui.base.BasePresenter
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

@PerView
class MainActivityPresenter @Inject constructor(private val dataManager: DataManager) : BasePresenter<MainActivityData>() {

    companion object {
        val TAG = MainActivityPresenter::class.java.simpleName
    }

    fun fetchCarList() {
        disposables.add(
                dataManager.getAllAvailableVehicles()
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(
                                { onFetchSuccess(it) },
                                { onFetchError(it) }
                        )
        )
    }

    private fun onFetchError(error: Throwable) {
        Log.e(TAG, "Failure fetching data", error)
    }

    private fun onFetchSuccess(cars: List<VehicleListResponse>) {
        Filters.carList = cars
        Filters.carListFiltered.onNext(cars)
    }

}