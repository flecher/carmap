package com.kanzi.carmap.ui.filter

import android.os.Bundle
import android.view.View
import com.kanzi.carmap.R
import com.kanzi.carmap.ui.base.BaseFragment
import kotlinx.android.synthetic.main.filter_layout.*

class FilterFragment : BaseFragment(), FilterView {

    override fun getLayoutResId(): Int = R.layout.filter_layout

    lateinit var presenter: FilterPresenter

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        fragmentComponent?.inject(this)
        presenter = FilterPresenter()
        presenter.bind(this, mRootView.context)
        init()
    }


    fun init() {
        ascPlateBtn.setOnClickListener {
            presenter.filterByPlate(true)
        }
        dscPlateBtn.setOnClickListener {
            presenter.filterByPlate(false)
        }

        ascBatteryBtn.setOnClickListener {
            presenter.filterByBattery(true)
        }
        dscBatteryBtn.setOnClickListener {
            presenter.filterByBattery(false)
        }

        ascDistanceBtn.setOnClickListener {
            presenter.filterByDistance(true)
        }
        dscDistanceBtn.setOnClickListener {
            presenter.filterByDistance(false)
        }
    }
}