package com.kanzi.carmap.ui.filter

import android.util.Log
import com.kanzi.carmap.data.Filters
import com.kanzi.carmap.di.scope.PerView
import com.kanzi.carmap.ui.base.BasePresenter

@PerView
class FilterPresenter : BasePresenter<FilterView>() {

    companion object {
        val TAG = FilterPresenter::class.java.simpleName
    }

    fun filterByPlate(sorting: Boolean) {
        Log.w(TAG, "car: " + Filters.carList.map { it.plateNumber })
        if (sorting) {
            Filters
                    .carListFiltered
                    .onNext(
                            Filters
                                    .carList
                                    .sortedByDescending { it.plateNumber }
                    )
        } else {
            Filters
                    .carListFiltered
                    .onNext(
                            Filters
                                    .carList
                                    .sortedByDescending { it.plateNumber }
                                    .reversed()
                    )
        }
    }

    fun filterByBattery(sorting: Boolean) {
        if (sorting) {
            Filters
                    .carListFiltered
                    .onNext(
                            Filters.carList
                                    .sortedByDescending { it.batteryPercentage }
                    )
        } else {
            Filters
                    .carListFiltered
                    .onNext(
                            Filters.carList
                                    .sortedByDescending { it.batteryPercentage }
                                    .reversed()
                    )
        }
    }

    fun filterByDistance(sorting: Boolean) {
        if (sorting) {

        } else {

        }
    }
}