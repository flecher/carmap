package com.kanzi.carmap.ui.map

import com.kanzi.carmap.data.Filters
import com.kanzi.carmap.data.model.VehicleListResponse
import com.kanzi.carmap.di.scope.PerView
import com.kanzi.carmap.ui.base.BasePresenter
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

@PerView
class MapPresenter : BasePresenter<MapView>() {

    companion object {
        val TAG = MapPresenter::class.java.simpleName
    }

    fun onResume() {
        Filters.carListFiltered
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        { onFetchSuccess(it) },
                        { onFetchError(it) }
                )
    }

    private fun onFetchSuccess(cars: List<VehicleListResponse>) {
        view?.clearMap()
        for (car in cars) {
            view?.showCarsOnMap(car)
        }
    }

    private fun onFetchError(error: Throwable) {
        view?.onFetchError(error)
    }
}