package com.kanzi.carmap.ui.list

import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import com.kanzi.carmap.R
import com.kanzi.carmap.adapters.CarListAdapter
import com.kanzi.carmap.data.model.VehicleListResponse
import com.kanzi.carmap.ui.base.BaseFragment
import com.kanzi.carmap.ui.map.MapFragment
import kotlinx.android.synthetic.main.car_list_layout.*

class ListFragment: BaseFragment(), ListView {

    override fun getLayoutResId(): Int = R.layout.car_list_layout

    lateinit var presenter: ListPresenter

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        fragmentComponent?.inject(this)
        presenter = ListPresenter()
        presenter.bind(this, mRootView.context)
        presenter.onResume()
    }

    override fun showCarList(cars : List<VehicleListResponse>) {
        carList.adapter = CarListAdapter(mRootView.context, cars)
        carList.deferNotifyDataSetChanged()
        carList.invalidate()
    }

    override fun onFetchError(error: Throwable) {
        Log.e(MapFragment.TAG, "Failure fechting data", error)
        Toast.makeText(mRootView.context, "Failure fetching data", Toast.LENGTH_LONG).show()
    }
}