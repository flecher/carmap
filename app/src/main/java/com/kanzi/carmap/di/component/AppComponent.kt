package com.kanzi.carmap.di.component

import com.kanzi.carmap.di.module.Bindings
import com.kanzi.carmap.di.module.NetworkModule
import dagger.Component
import javax.inject.Singleton


@Singleton
@Component(modules = [(NetworkModule::class), (Bindings::class)])
interface AppComponent {
    fun activityComponent(): ActivityComponent

    fun fragmentComponent(): FragmentComponent
}