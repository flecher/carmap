package com.kanzi.carmap.di.component

import com.kanzi.carmap.di.scope.PerView
import com.kanzi.carmap.ui.MainActivity

import dagger.Subcomponent


@PerView
@Subcomponent
interface ActivityComponent {
    fun inject(activity: MainActivity)
}