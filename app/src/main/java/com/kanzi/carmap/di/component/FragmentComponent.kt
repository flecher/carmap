package com.kanzi.carmap.di.component


import com.kanzi.carmap.di.scope.PerView
import com.kanzi.carmap.ui.filter.FilterFragment
import com.kanzi.carmap.ui.list.ListFragment
import com.kanzi.carmap.ui.map.MapFragment
import dagger.Subcomponent

@PerView
@Subcomponent
interface FragmentComponent {

    fun inject(fragment: ListFragment)

    fun inject(fragment: MapFragment)

    fun inject(fragment: FilterFragment)
}