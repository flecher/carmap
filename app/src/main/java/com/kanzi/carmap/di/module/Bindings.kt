package com.kanzi.carmap.di.module

import com.kanzi.carmap.data.DataManager
import com.kanzi.carmap.data.DataManagerImpl
import dagger.Binds
import dagger.Module

@Module
abstract class Bindings {

    @Binds
    internal abstract fun bindDataManager(manager: DataManagerImpl): DataManager
}