package com.kanzi.carmap.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import com.bumptech.glide.Glide
import com.kanzi.carmap.R
import com.kanzi.carmap.data.model.VehicleListResponse

class CarListAdapter(
        private val context: Context,
        private val items: List<VehicleListResponse>) : BaseAdapter() {

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val view: View?
        val viewHolder: CarListViewHolder
        if (convertView == null) {
            val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            view = inflater.inflate(R.layout.car_list_row, null)
            viewHolder = CarListViewHolder(view)
            view?.tag = viewHolder
        } else {
            view = convertView
            viewHolder = view.tag as CarListViewHolder
        }

        val car = items[position]
        viewHolder.plateNumberText?.text = car.plateNumber
        viewHolder.carTitleText?.text = car.model.title
        Glide
                .with(context)
                .load(car.model.photoUrl)
                .into(viewHolder.carImage)
        viewHolder.carImage
        return view as View
    }

    override fun getItem(position: Int): Any {
        return items[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getCount(): Int {
        return items.size
    }
}