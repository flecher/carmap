package com.kanzi.carmap.adapters

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.kanzi.carmap.R

class CarListViewHolder(row: View?) {
    var carTitleText: TextView? = null
    var plateNumberText: TextView? = null
    var carImage: ImageView? = null

    init {
        carImage = row?.findViewById(R.id.carImage)
        carTitleText = row?.findViewById(R.id.carTitleText)
        plateNumberText = row?.findViewById(R.id.plateNumberText)
    }
}