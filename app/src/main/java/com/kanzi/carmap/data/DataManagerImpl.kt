package com.kanzi.carmap.data

import com.kanzi.carmap.data.model.VehicleListResponse
import com.kanzi.carmap.data.remote.CarListService
import io.reactivex.Observable
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
internal class DataManagerImpl @Inject constructor(private val carListService: CarListService) : DataManager {

    override fun getAllAvailableVehicles(): Observable<List<VehicleListResponse>> {
        return carListService.getAllAvailableVehicles()
    }
}