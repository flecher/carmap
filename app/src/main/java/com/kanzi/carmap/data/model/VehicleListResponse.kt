package com.kanzi.carmap.data.model


data class VehicleListResponse(val id: Int,
                               val plateNumber: String,
                               val location: CarLocation,
                               val model: CarModel,
                               val batteryPercentage: Int,
                               val batteryEstimatedDistange: Int,
                               val isCharging: Boolean)

data class CarLocation(val id: Int,
                       val latitude: Double,
                       val longitude: Double,
                       val address: String)

data class CarModel(val id: Int,
                    val title: String,
                    val photoUrl: String)
