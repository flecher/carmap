package com.kanzi.carmap.data

import com.kanzi.carmap.data.model.VehicleListResponse
import io.reactivex.Observable

interface DataManager {

    fun getAllAvailableVehicles(): Observable<List<VehicleListResponse>>
}