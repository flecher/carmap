package com.kanzi.carmap.data

import com.kanzi.carmap.data.model.VehicleListResponse
import io.reactivex.subjects.BehaviorSubject

class Filters {
    companion object {
        var test: String = ""
        var carListFiltered: BehaviorSubject<List<VehicleListResponse>> = BehaviorSubject.create()
        var carList: List<VehicleListResponse> = ArrayList()
    }
}