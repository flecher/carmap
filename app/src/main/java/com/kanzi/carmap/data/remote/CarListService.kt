package com.kanzi.carmap.data.remote

import com.kanzi.carmap.data.model.VehicleListResponse
import io.reactivex.Observable
import retrofit2.http.GET

interface CarListService {

    @GET("api/mobile/public/availablecars")
    fun getAllAvailableVehicles(): Observable<List<VehicleListResponse>>
}