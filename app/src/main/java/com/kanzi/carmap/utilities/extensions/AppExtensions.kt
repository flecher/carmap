package com.kanzi.carmap.utilities.extensions

import android.content.Context
import com.kanzi.carmap.MainApp
import com.kanzi.carmap.di.component.AppComponent

fun Context.getAppComponent(): AppComponent = (applicationContext as MainApp).appComponent