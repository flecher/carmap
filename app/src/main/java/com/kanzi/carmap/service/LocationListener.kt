package com.kanzi.carmap.service

import android.location.Location
import android.location.LocationListener
import android.os.Bundle
import android.util.Log

class LocationListener(provider: String) : LocationListener {

    companion object {
        val TAG = LocationListener::class.java.simpleName
    }

    var mLastLocation: Location = Location(provider)

    override fun onLocationChanged(location: Location?) {
        mLastLocation.set(location)
    }

    override fun onStatusChanged(provider: String?, status: Int, extras: Bundle?) {
        Log.e(TAG, "on status changed: $provider")
    }

    override fun onProviderEnabled(provider: String?) {
        Log.i(TAG, "provider enabled: $provider")
    }

    override fun onProviderDisabled(provider: String?) {
        Log.e(TAG, "provider disabled: $provider")
    }
}