package com.kanzi.carmap.service

import android.annotation.SuppressLint
import android.app.Service
import android.content.Context
import android.content.Intent
import android.location.LocationManager
import android.os.IBinder
import android.util.Log
import java.lang.Exception
import java.lang.IllegalArgumentException

class LocationService : Service() {

    companion object {
        val TAG = LocationService::class.java.simpleName
        private val LOCATION_INTERVAL: Long = 1000
        private val LOCATION_DISTANCE: Float = 10f
    }

    private lateinit var mLocationManager: LocationManager

    override fun onCreate() {
        init()
    }

    private fun init() {
        mLocationManager = applicationContext
                .getSystemService(Context.LOCATION_SERVICE) as LocationManager
        initLocationUpdates()
    }

    @SuppressLint("MissingPermission")
    private fun initLocationUpdates() {
        try {
            mLocationManager
                    .requestLocationUpdates(
                            LocationManager.NETWORK_PROVIDER,
                            LOCATION_INTERVAL,
                            LOCATION_DISTANCE,
                            LocationListener(LocationManager.NETWORK_PROVIDER))
        } catch (ex : SecurityException) {
            Log.e(TAG, "Failure requesting location updates", ex)
        } catch (ex : IllegalArgumentException) {
            Log.e(TAG, "Network provider does not exist", ex)
        }
    }

    override fun onBind(intent: Intent?): IBinder? {
        return null
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        super.onStartCommand(intent, flags, startId)
        return START_STICKY
    }

    override fun onDestroy() {
        super.onDestroy()
        try {
            mLocationManager.removeUpdates(LocationListener(LocationManager.NETWORK_PROVIDER))
        } catch (ex: Exception) {
            Log.e(TAG, "Failure removing location listener", ex)
        }

    }
}