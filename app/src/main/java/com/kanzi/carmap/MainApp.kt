package com.kanzi.carmap

import android.app.Application
import com.kanzi.carmap.di.component.AppComponent
import com.kanzi.carmap.di.component.DaggerAppComponent

class MainApp : Application() {

    lateinit var appComponent: AppComponent

    override fun onCreate() {
        super.onCreate()
        setupComponent()
    }

    private fun setupComponent() {
        appComponent = DaggerAppComponent.builder().build()
    }
}